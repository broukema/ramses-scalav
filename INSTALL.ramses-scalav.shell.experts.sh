#!/bin/bash

# ramses-scalav.shell.experts script (C) 2017-2018 B. Roukema GPLv2 or later

# This script should install mpgrafic, DTFE, inhomog, RAMSES and ramses-scalav
# and run a small-number demonstration VQZA simulation. See Roukema 2018
# A&A 610, A51, arXiv:1706.06179 for details.
# As of 2018, this file is distributed at https://bitbucket.org/broukema/ramses-scalav .

echo "This script is more or less equivalent to INSTALL.ramses-scalav,"
echo "but instead is written as an executable bash script. It will most likely FAIL on"
printf "your system! Do not say that you have not been warned!\n"
echo "A safer option is to work through the steps of INSTALL.ramses-scalav"
echo "one by one, checking that you understand all the steps."

printf "\nIf you really want to run this, then follow this Usage:\n\n"
printf "$0 DIRECTORY YES\n\n"
printf "where DIRECTORY is the directory where you want to install\n"
printf "everything (e.g. ${PWD}), and YES is literally the string YES.\n"
printf "\nHowever, you should first read through the script and check\n"
printf "whether it can reasonably be expected to do what you would like it\n"
printf "to do.\n\n"

printf "Comments: This script is a temporary hack pending proper packaging of\n"
printf "the various libraries that are needed. It is *not* in itself a serious\n"
printf "long-term solution.\n"

MPGRAFIC_COMMIT_HASH=7cf9861
DTFE_COMMIT_HASH=a9db229
INHOMOG_COMMIT_HASH=0d6e028
RAMSES_COMMIT_HASH=a317f07
RAMSES_COMMIT_BRANCH2=allow_64bit_int_postrum2017
RAMSES_COMMIT_HASH2=7b64713
RAMSES_SCALAV_COMMIT_HASH=c60437f

## avoid LDFLAGS apart from those strictly needed
OLD_LDFLAGS=${LDFLAGS}
LDFLAGS=

## The script will run faster without checks, but you are less likely to detect
## portability/software evolution problems or other bugs early if you turn this off.
DO_CHECKS=YES
#DO_CHECKS=NO

## Try an existing set of packages installed in the same top directory?
TRY_OLD=YES
#TRY_OLD=NO

## three parameters for bitbucket repository: DEVELOPER PACKAGE HASH
function clone_bitbucket {
    if [ "x${TRY_OLD}" = "xYES" ]; then
        cd $2 && git pull
        cd ..
    else
        git clone https://bitbucket.org/$1/$2
    fi
    cd $2
    WE_HAVE=$(git log --oneline |head -n 1 | awk '{print $1}')
    if [ "x${WE_HAVE}" = "x$3" ]; then
        echo "The git clone and commit version seem to be correct."
    else
        # Try to checkout the required version and test again.
        git checkout $3
        WE_HAVE=$(git log --oneline |head -n 1 | awk '{print $1}')
        if [ "x${WE_HAVE}" = "x$3" ]; then
            echo "The git clone and checked out commit seem to be correct."
        else
            echo "The git clone or commit version seem to have a problem."
            cd .. # return to root install directory
            exit 1
        fi
    fi
    cd .. # return to root install directory
}


if [ $# -eq 0 ]; then
    exit 0
fi

FULL_INSTALL_DIR=$1

if [ ! -d "${FULL_INSTALL_DIR}" ]; then
    printf "Cannot find directory '${FULL_INSTALL_DIR}'.\n"
    exit 1
fi
cd ${FULL_INSTALL_DIR}
export MYUSR=$(/bin/pwd)
cd ${OLDPWD}
printf "\nI will install everything in '${MYUSR}'.\n"

if [ $# -lt 2 ]; then
    printf "But first you must retype the command with YES as the second argument.\n"
    exit 0
fi

REALLY=$2

if [ "x${REALLY}" = "xYES" ] ; then


    # 0. ENVIRONMENT
    #
    #    Set up or choose a user space directory tree for libraries,
    #    include files, binaries (and in some cases, man pages). This
    #    should be throwaway space that is not backed up, since all
    #    these files can be trivially re-created.
    #    Define an environment variable for this, e.g.

    mkdir -p ${MYUSR} ${MYUSR}/lib ${MYUSR}/include ${MYUSR}/bin

    #   You will then need to update environment variables based on
    #    this main directory:

    export PATH=${PATH}:${MYUSR}/bin
    export MANPATH=${MANPATH}:${MYUSR}/share/man
    export LDFLAGS="${LDFLAGS} -L${MYUSR}/lib"
    export FFLAGS="${FFLAGS} -I${MYUSR}/include"

    #    These variables are assumed to be defined in each of the shell
    #    sessions in which you work below.
    #
    #
    # A. MPGRAFIC:
    #
    # *In Debian/Ubuntu:*
    #  Check distribution availability:
    #
    #    https://tracker.debian.org/pkg/mpgrafic
    #    https://launchpad.net/ubuntu/+source/mpgrafic
    #
    # and then
    #
    #  aptitude install mpgrafic
    #  man mpgrafic
    #
    # *In other unixlike systems:*
    #
    # 1 Download from https://bitbucket.org/broukema/mpgrafic .

    clone_bitbucket broukema mpgrafic ${MPGRAFIC_COMMIT_HASH}

    # 2.1 Adapt Makefile.am and run autoconf if necessary.
    #
    # 2.2 Install system libraries, e.g. on Debian/stretch, if needed.
    #
    #
    for NEED_LIB in libopenmpi-dev openmpi-bin fftw-dev; do
        if (dpkg -l | grep "^ii *${NEED_LIB}") ; then
            echo "...found";
        else
            echo "${NEED_LIB} is not yet present in your system. You will be"
            echo "asked in a moment to enter your password for installing ${NEED_LIB}"
            echo "with sudo. If you do not trust this script, then ^C and check it again"
            echo "before proceeding with caution."
            sudo aptitude install ${NEED_LIB}
        fi
    done
    #
    # 3. Configure and compile

    cd mpgrafic
    ./configure --prefix=${MYUSR} && make

    if [ "x${DO_CHECKS}" = "xYES" ]; then
        # to check your installation
        if (make check); then
            printf "mpgrafic passed its check.\n"
        else
            printf "mpgrafic failed its check.\n"
            exit 1
        fi
    fi

    make install  # to install mpgrafic in ${MYUSR}/bin/

    cd ../


    # B. DTFE-1.1.1.Q:
    #
    # 1. Download from https://bitbucket.org/broukema/dtfe

    clone_bitbucket broukema dtfe ${DTFE_COMMIT_HASH}

    # 2.1 Adapt Makefile if necessary.
    #
    # 2.2 Install system libraries, e.g. on Debian/jessie:

    for NEED_LIB in libcgal-dev libcgal12 libboost-filesystem-dev; do
        if (dpkg -l | grep "^ii *${NEED_LIB}") ; then
            echo "...found";
        else
            echo "${NEED_LIB} is not yet present in your system. You will be"
            echo "asked in a moment to enter your password for installing ${NEED_LIB}"
            echo "with sudo. If you do not trust this script, then ^C and check it again"
            echo "before proceeding with caution."
            sudo aptitude install ${NEED_LIB}
        fi
    done


    # 3. To check your installation:

    cd dtfe
    if [ "x${DO_CHECKS}" = "xYES" ]; then
        make tests # && ./test_vgrad_DTFE # TODO: should become "make check"
        if ( ./test_vgrad_DTFE ); then
            printf "DTFE passed its check.\n"
        else
            printf "DTFE failed its check.\n"
            exit 1
        fi
    fi

    #    The test should take about a minute on an 8-core machine.

    make library_static # to make the static library

    cp -pv libDTFE.a ${MYUSR}/lib/

    cd ../

    #exit 0 # debug


    # C. INHOMOG:
    #
    # 1. Download from https://bitbucket.org/broukema/inhomog

    clone_bitbucket broukema inhomog ${INHOMOG_COMMIT_HASH}

    cd inhomog/
    ./configure --prefix=${MYUSR} --disable-shared && make

    # 2.2 If necessary, use autoreconf and repeat 2.1 .

    # 3. Check the compiled files.

    #   make check # should take about 4 minutes on a 2-core machine
    if [ "x${DO_CHECKS}" = "xYES" ]; then
        if ( make check ); then
            printf "inhomog passed its checks.\n"
        else
            printf "inhomog failed its checks.\n"
            exit 1
        fi
    fi

    # 4. To install libinhomog in ${MYUSR}/lib and
    #   inhomog in ${MYUSR}/bin, do

    make install


    # 5. You can now run biscale scale factor VQZA and EdS calculations:

    #   man inhomog
    inhomog --help

    cd ../

    #exit 0 # debug


    # D. RAMSES:

    # 1. Git download from https://bitbucket.org/rteyssie/ramses

    #git clone https://bitbucket.org/rteyssie/ramses

    clone_bitbucket broukema ramses-use-mpif08 ${RAMSES_COMMIT_HASH}

    cd ramses-use-mpif08/
    git checkout ${RAMSES_COMMIT_BRANCH2} || (echo "Could not find branch" && exit 1)
    WE_HAVE=$(git log --oneline |head -n 1 | awk '{print $1}')
    if [ "x${WE_HAVE}" = "x${RAMSES_COMMIT_HASH2}" ]; then
        echo "The git commit version seems to be correct."
    else
        echo "The git commit version seems to have a problem."
        cd .. # return to root install directory
        exit 1
    fi
    cd .. # return to root install directory

    ln -s ramses-use-mpif08 ramses # symbolic link to use generic ramses name

    cd ramses


    # 2. Follow RAMSES documentation for standard installation and testing.
    #    You will very likely want to modify Makefile a little; you can
    #    later look at the differences between your version and the upstream
    #    version with
    #
    #    git diff Makefile
    #    git diff --word-diff Makefile

    # 3. git checkout a version known to work with ramses-scalav:

    git checkout ${RAMSES_COMMIT_HASH2} # && git log --stat

    cd ../


    #exit 0 # debug


    # E. ramses-scalav:

    # 1. Git download from https://bitbucket.org/broukema/ramses-scalav

    clone_bitbucket broukema ramses-scalav ${RAMSES_SCALAV_COMMIT_HASH}


    # 2. Copy the mpgrafic script into your binary directory:

    cd ramses-scalav
    cp -pv mpgrafic_params/mpgrafic_params.sh ${MYUSR}/bin

    # 3. Make a working directory "EdSrefmodel" (EdS reference model) and
    #    run the mpgrafic_params.sh script:

    cd ../
    mkdir EdSrefmodel
    cd EdSrefmodel
    mpgrafic_params.sh 40 32  # 40 Mpc/h0eff boxsize, N=64^3

    #    You should now have several initial conditions files in EdSrefmodel.
    #
    # 4. Change to your RAMSES directory, symbolically link your
    #    ramses-scalav directory inside of patch/, and symbolically
    #    link the scalav Makefile:
    #
    cd ../ramses/patch/
    ln -s ../../ramses-scalav scalav # give the symbolic name "scalav"
    cd ../bin
    ln -s ../patch/scalav/Makefile.scalav .

    # 5. Adapt Makefile.scalav if necessary. You will probably want to make
    #    changes similar to those you made for the RAMSES Makefile.
    #
    # 6. Compile and install:

    make -f Makefile.scalav
    cp -pv ramses3d ${MYUSR}/bin

    # 7. Change back to your EdSrefmodel directory and copy the namelist file
    #    with simulation parameters there:

    cd ../../EdSrefmodel
    # Make a local copy of the simulation parameter file:
    cp -pv ../ramses-scalav/namelist/cosmo.nml.EdSrefmodel.32cubed .


    # 8. Run ramses-scalav, e.g. with 4 cores for MPI, and unlimited cores
    #    for openmp (DTFE), saving the results to a file, e.g.

    export RAMSES_SCALAV_LOG_FILE="scalav.$(date +%Y%m%d-%H%M%S).log"
    mpirun -n 4 ramses3d cosmo.nml.EdSrefmodel.32cubed > ${RAMSES_SCALAV_LOG_FILE}

    # 9. Install plotutils or use another graphical program to plot the results:

    for NEED_LIB in plotutils; do
        if (dpkg -l | grep "^ii *${NEED_LIB}") ; then
            echo "...found";
        else
            echo "${NEED_LIB} is not yet present in your system. You will be"
            echo "asked in a moment to enter your password for installing ${NEED_LIB}"
            echo "with sudo. If you do not trust this script, then ^C and check it again"
            echo "before proceeding with caution."
            sudo aptitude install ${NEED_LIB}
        fi
    done


    (grep ^a_D_global ${RAMSES_SCALAV_LOG_FILE} |awk '{print $2,$3}' ; \
     printf "\n"; \
     grep ^a_D_global ${RAMSES_SCALAV_LOG_FILE} |awk '{print $2,$5}' ) |\
        graph -TX -X "t (Gyr)" -Y "a(t) [EdS or eff]"

fi #  if [ "x${REALLY}" = "xYES" ] ; then

## restore original LDFLAGS in case the value is exported back out
LDFLAGS=${OLD_LDFLAGS}
