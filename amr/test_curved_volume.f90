! test_curved_volume - convert flat volume to curved, using curvature radius
! Copyright (C) 2016 B. Roukema  under the Cecill licence
! See http://www.cecill.info for licence details; the licence is
! GPL-compatible.


! Given a Euclidean 3-volume vol_flat, calculate the effective radius,
! i.e. the radius assuming that the 3-volume is bounded by a 2-sphere,
! and then output the spherical or hyperbolic volume to that radius.
! A negative return value indicates an error.

program test_curved_volume
  use curved_volume_module

  implicit none

  logical :: curved_volume_OK = .true.
  logical :: verbose = .true.

  real(dp), parameter :: TOL1 = 1e-4
  real(dp), parameter :: TOL2 = 1e-14

  real(dp) :: vol_flat(4)
  real(dp) :: vol_curv(4)
  real(dp) :: R_C

  ! ratio should approach 1 for high curvature radius
  vol_flat(1) = 10.0_dp
  vol_flat(2) = 10.0_dp
  vol_flat(3) = 10.0_dp
  R_C = 100.0
  vol_curv(1) = curved_volume_3D(vol_flat(1), R_C, 1) ! spherical
  vol_curv(2) = curved_volume_3D(vol_flat(2), R_C, -1) ! hyperbolic
  vol_curv(3) = curved_volume_3D(vol_flat(2), R_C, 0) ! flat
  if( max(abs(vol_curv(1)/vol_flat(1) - 1.0_dp), &
       abs(vol_curv(2)/vol_flat(2) -1.0_dp), &
       abs(vol_curv(3)/vol_flat(3) -1.0_dp) &
       ) > TOL1 ) then
     curved_volume_OK = .false.
     if(verbose)then
        print*,'vol_curv(1), vol_flat(1) = ',vol_curv(1), vol_flat(1)
        print*,'vol_curv(2), vol_flat(2) = ',vol_curv(2), vol_flat(2)
        print*,'vol_curv(3), vol_flat(3) = ',vol_curv(3), vol_flat(3)
     endif
  endif

  ! check if S^3 volume out to pi R_C/2 is pi^2 R_C^3
  vol_flat(4) = 4.0_dp/3.0_dp *PI * (PI/2.0_dp * R_C)**3
  vol_curv(4) = curved_volume_3D(vol_flat(4), R_C, 1)
  if( abs( vol_curv(4)/ (PI**2 * R_C**3) - 1.0_dp ) > TOL2 )then
     curved_volume_OK = .false.
     if(verbose)then
        print*,'vol_curv(4), (PI**2 * R_C**3) = ',vol_curv(4), (PI**2 * R_C**3)
     endif
  endif

  if(curved_volume_OK)then
     print*,'curved_volume passed its tests'
  else
     print*,'curved_volume FAILED its tests'
  endif
endprogram
