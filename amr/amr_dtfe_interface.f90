! amr_dtfe_interface - interface between RAMSES and DTFE
! Copyright (C) 2016, 2017 B. Roukema under the Cecill licence
! See http://www.cecill.info for licence details.

subroutine amr_dtfe_interface
  use amr_commons
  use pm_commons
  use curved_volume_module
  use update_av_scalars_global_module
  use update_time_module

#ifndef WITHOUTMPI
#ifndef OLD_MPI_F77_STYLE
  use mpi_f08
#endif
#endif
  implicit none
#ifndef WITHOUTMPI
#ifdef OLD_MPI_F77_STYLE
  include 'mpif.h'
#endif
#endif

  !----------------------------------------------------
  ! Scalar averaging
  !----------------------------------------------------
  ! Scalar averaging (general-relativistic) volume evolution constraint.
  ! Call DTFE (Delaunay Tesselation Field Estimator) to
  ! calculate the kinematical backreaction; and call update_av_scalars_global
  ! to estimate the global effective expansion rate. The present algorithm
  ! does both steps globally; hence the MPI_BARRIER.

  interface
     subroutine DTFE_f90_wrapper(npart_tot, &
          boxsize, dtfe_averaging_method, dtfe_n_gridsize_to_wrapper, &
          xp_global, vp_global, mp_global, &
          dtfe_velocity, &
          divergence, IIinv, IIIinv, &
          Q_kin_backreaction, Q_sigma, &
          shear, vorticity, &
          dtfe_density) !bind (c)

       !use iso_c_binding
       use amr_parameters ! See portability warning in amr_parameters.f90.
       ! INPUTS
       integer(i8b), intent(in) :: npart_tot
       real(dp), intent(in) :: boxsize
       integer(i8b), intent(in) :: dtfe_averaging_method
       integer(i8b), intent(in) :: dtfe_n_gridsize_to_wrapper
       real(dp), intent(in), allocatable,dimension(:,:)::xp_global   ! Positions
       real(dp), intent(in), allocatable,dimension(:,:)::vp_global   ! Velocities
       real(dp), intent(in), allocatable,dimension(:)::mp_global   ! Masses
       ! OUTPUTS
       ! These arrays must be allocated before calling DTFE_f90_wrapper.
       real(dp), intent(inout), allocatable,dimension(:,:,:,:) :: dtfe_velocity ! DT smoothed velocity
       real(dp), intent(inout), allocatable,dimension(:,:,:) :: divergence ! of vel gradient
       real(dp), intent(inout), allocatable,dimension(:,:,:) :: IIinv ! second invariant of vel gradient
       real(dp), intent(inout), allocatable,dimension(:,:,:) :: IIIinv ! third invariant of vel gradient
       ! kinematical backreaction map:
       real(dp), intent(inout), allocatable, dimension(:,:,:) :: Q_kin_backreaction
       real(dp), intent(inout), allocatable, dimension(:,:,:) :: Q_sigma
       ! shear map:
       real(dp), intent(inout), allocatable, dimension(:,:,:) :: shear
       ! vorticity map:
       real(dp), intent(inout), allocatable, dimension(:,:,:) :: vorticity
       ! density map:
       real(dp), intent(inout), allocatable, dimension(:,:,:) :: dtfe_density

     end subroutine DTFE_f90_wrapper
  end interface

  interface
     ! Calculate RZA scale factor evolution and evolution of
     ! effective Omegas, given a set of initial values of the
     ! extrinsic curvature invariants calculated on a set of
     ! subdomains whose disjoint union is the total domain
     ! (arXiv:1303.6193).
     !
     ! TODO: This interface should be shifted to the inhomog library.
     subroutine Omega_D_precalc( &! INPUTS
          scal_av_n_gridsize1_wrap, &
          scal_av_n_gridsize2_wrap, &
          scal_av_n_gridsize3_wrap, &
          divergence_av_wrap, IIinv_av_wrap, IIIinv_av_wrap, &
          aD_norm_wrap, &
          n_t_inhomog_wrap, &
          aexp_wrap, &
          h0_wrap, &
          ! OUTPUTS
          t_RZA_wrap, a_D_RZA_wrap &
          ) bind (C, name='Omega_D_precalc')

       use amr_parameters
       use iso_c_binding

       implicit none

       ! INPUTS
       ! See amr/amr_parameters.f90 for the meanings of these parameters:
       integer(c_int64_t), intent(in) :: scal_av_n_gridsize1_wrap ! cf amr_parameters
       integer(c_int64_t), intent(in) :: scal_av_n_gridsize2_wrap ! cf amr_parameters
       integer(c_int64_t), intent(in) :: scal_av_n_gridsize3_wrap ! cf amr_parameters

       ! These arrays must be allocated before calling Omega_D_precalc:
       ! kinematical backreaction map
       real(c_double), intent(in), allocatable,dimension(:,:,:) :: divergence_av_wrap ! of vel gradient
       real(c_double), intent(in), allocatable,dimension(:,:,:) :: IIinv_av_wrap ! second invariant of vel gradient
       real(c_double), intent(in), allocatable,dimension(:,:,:) :: IIIinv_av_wrap ! third invariant of vel gradient

       real(c_double), intent(in), allocatable,dimension(:,:,:) :: aD_norm_wrap ! initial timestep normalisation of per-domain scale factors

       integer(c_int64_t), intent(in) :: n_t_inhomog_wrap ! cf amr_parameters
       real(c_double), intent(in) :: aexp_wrap, h0_wrap ! cf amr_parameters

       ! OUTPUTS
       ! These arrays must be allocated before calling Omega_D_precalc:
       real(c_double), intent(inout), allocatable, dimension(:) &
            :: t_RZA_wrap, a_D_RZA_wrap

     end subroutine Omega_D_precalc
  end interface

  ! TODO: Warn RAMSES group about other parts of RAMSES: idim has been
  ! a fortran function since f77 - competing with an existing function
  ! could be risky with some compilers:
  ! https://gcc.gnu.org/onlinedocs/gfortran/DIM.html
  integer(i4b)::i_dim

  integer(i8b) :: i_part, ix,iy,iz
  integer(i8b) :: ixx,iyy,izz ! for div^2
  !TODO: consistency of integer types: ramses-scalav, inhomog, DTFE
  integer(i8b) :: npart_tot

#define TOL_VOL_AMR_DTFE (1d-20)

#ifndef WITHOUTMPI
  integer(i8b) :: i_part_valid ! particle iterator - valid particles
  integer(i8b) :: npart_loc ! local (per cpu) number of particles

  integer(i4b) :: ierr_scalav
  integer(i4b) :: i_mpi_rank! cpu identity
  integer(i4b) :: i_mpi_rank_other ! cpu identity
  ! tags to uniquely identify messages passed via MPI
  integer(i4b) :: tag_npart = 3010, tag_xp = 3020, tag_vp = 3030
  integer(i4b) :: tag_mp = 3040, tag_levelp = 3050, one=1, zero=0
#endif /* WITHOUTMPI */

  ! These are global for the full simulation at a given time step;
  ! they are gathered here and fed to DTFE_f90_wrapper.
  real(dp),allocatable,dimension(:,:)::xp_global   ! Positions
  real(dp),allocatable,dimension(:,:)::vp_global   ! Velocities
  real(dp),allocatable,dimension(:)::mp_global   ! Masses
  integer, allocatable,dimension(:) ::levelp_global ! Current particle level
  integer(i8b) :: npart_cumul ! number of particles cumulated from several cpus

#ifndef WITHOUTMPI
  ! variables for an individual cpu
  real(dp),allocatable,dimension(:,:)::xp_local   ! Positions
  real(dp),allocatable,dimension(:,:)::vp_local   ! Velocities
  real(dp),allocatable,dimension(:)::mp_local   ! Masses
  integer, allocatable,dimension(:) ::levelp_local ! Current particle level
  integer(i8b) :: npart_partial
#endif /* WITHOUTMPI */

  ! TODO: allow unequal masses
  !real(dp),allocatable,dimension(:)  ::mp_global       ! Masses

  ! These arrays must be allocated before calling DTFE_f90_wrapper.
  real(dp),allocatable,dimension(:,:,:,:) :: dtfe_velocity ! DT smoothed velocity
  real(dp),allocatable,dimension(:,:,:) :: divergence ! of vel gradient
  real(dp),allocatable,dimension(:,:,:) :: IIinv ! second invariant of vel gradient
  real(dp),allocatable,dimension(:,:,:) :: IIIinv ! third invariant of vel gradient
  real(dp),allocatable,dimension(:,:,:) :: Q_kin_backreaction
  real(dp),allocatable,dimension(:,:,:) :: Q_sigma
  real(dp),allocatable,dimension(:,:,:) :: shear ! scalar
  real(dp),allocatable,dimension(:,:,:) :: vorticity ! scalar
  real(dp),allocatable,dimension(:,:,:) :: dtfe_density ! DTFE-resolution Omega_m^FLRW

  ! Effective Omega's as in RZA2 (18), (19), at DTFE resolution level
  logical, save :: have_old_H_D_hexp = .false.
  real(dp), save :: old_H_D_common, old_hexp_invGyr
  real(dp) :: H_sq_factor, minus_sixHD_sq_factor
  real(dp) :: vol_flat_tmp, R_curv_tmp
  integer :: sign_RC
  real(dp),allocatable,dimension(:,:,:) :: Omega_m_dtfe
  real(dp),allocatable,dimension(:,:,:) :: Omega_Q_dtfe
  real(dp),allocatable,dimension(:,:,:) :: Omega_R_dtfe
  real(dp),allocatable,dimension(:,:,:) :: vol_curved_dtfe

  real(dp) :: mean_density

  ! The Lagrangian method is for the N-body method only.
  ! The Eulerian method for N-body is low priority; do not
  ! expect it to work correctly.
  logical :: use_Lagrange = .true. ! better keep at .true.

  ! The DTFE cell coordinates ip, jp, kp, of the ipart-th particle
  ! are stored as
  ! ijk_Lagrangian(ipart,1),
  ! ijk_Lagrangian(ipart,2),
  ! ijk_Lagrangian(ipart,3),
  ! respectively.
  integer(i8b), save, allocatable, dimension(:,:) :: ijk_Lagrangian


  logical, save :: have_Lag_IDs = .false.
  integer(i8b),save, allocatable,dimension(:,:,:) :: n_per_Lag_cell
  real(dp) :: xx, dtfe_grid_per_box ! temporary work
  real(dp) :: scal_av_grid_per_box ! temporary work
  ! linear interpolation
  real(dp) :: density_interpolated
  real(dp) :: inv_vol_tmp, vol_tmp
  real(dp) :: frac_lo(3) ! fraction of dtfe cell in i_dim-th direction
  real(dp) :: frac_hi(3) ! 1 - frac_lo(.)
  integer(i8b) :: ip_lo(3) ! lower dtfe cell corner ID
  integer(i8b) :: ip_hi(3) ! upper dtfe cell corner ID

  integer(i8b) :: n_antiflow ! number of particles moving against the flow
  real(dp) :: vdtfe_dot_vpart ! dot product of particle velocity and DTFE velocity
  real(dp) :: vdtfe_interp
  real(dp) :: vpart_mod ! modulus of particle peculiar velocity
  real(dp) :: f_antiflow ! rough estimator of half of virialisation fraction

  ! Scalar AVerages, to be fed to Omega_D_precalc() in the inhomog library,
  ! or to update_av_scalars_global() in ramses-scalav.
  !
  ! divergence of vel gradient = its Ist invariant
  real(dp),allocatable,dimension(:,:,:) :: divergence_av
  real(dp),allocatable,dimension(:,:,:) :: divergence_sq_av
  real(dp) :: divergence_interp
  ! IInd invariant of vel gradient
  real(dp),allocatable,dimension(:,:,:) :: IIinv_av
  real(dp) :: IIinv_interp
  ! The IIIrd invariant is used by Omega_D_precalc in the inhomog
  ! library that carries out the RZA evolution option; but at the
  ! present is not used by update_av_scalars_global, since Q is
  ! calculated numerically in this case, not analytically.  The
  ! statistical structure information represented by IIIinv_av may
  ! nevertheless be useful in the update_av_scalars_global option in
  ! the future.
  real(dp),allocatable,dimension(:,:,:) :: IIIinv_av ! IIIrd invariant
  real(dp) :: IIIinv_interp
  real(dp),allocatable,dimension(:,:,:) :: shear_av
  real(dp) :: shear_interp
  real(dp),allocatable,dimension(:,:,:) :: vorticity_av
  real(dp) :: vorticity_interp
  real(dp),allocatable,dimension(:,:,:) :: dtfe_density_av ! local Omega_m
  real(dp),allocatable,dimension(:,:,:) :: Eucl_vol_tot
  integer, save :: i_warn_zero_vol = 0, max_warn_zero_vol = 3
  integer :: min_per_Lag_subdomain = 9999 ! check for empty Lagrangian subdomains



  ! DTFE parameters
  real(dp) :: boxsize = 1_dp
  integer(i8b) :: dtfe_averaging_method = 1

  ! convert first invariant:
  ! simulation units delta v/delta x-> inv Gyr
  real(dp) :: convert_I_invGyr
  ! divide convert_I_invGyr by H in invGyr -> dimensionless
  real(dp) :: convert_I_dimless
  logical, save :: first_hexp = .true.
  real(dp), save :: hexp_init_invGyr
  real(dp) :: sigma_omega_m_FLRW

#ifdef SCALAV_CHECK_V_SCALE
  integer, save :: i_time_step = 0
  real(dp), save :: t_FLRW(2) ! times in ordinary FLRW Gyr, apart from t_0 offset
  real(dp) :: vv ! temporary work

  ! Number of particles with non-negligible velocities, i.e. which are
  ! usable for this test.
  integer(i8b) :: ipart_OK

  ! big time steps: FLRW, recalculated mean and stand. dev.
  real(dp) :: dt_FLRW, dt_recalc, dt_recalc_sig
#endif /* SCALAV_CHECK_V_SCALE */

  ! 1 Mpc/Gyr in terms of km/s
  real(dp), parameter :: kms_per_MpcGyr = 977.79_dp
  real(dp), parameter :: c_spacetime_convert_kms = 299792.458
#define TOL_CURV (1d-10)

  real(dp)::scale_l_kpc,scale_t_Gyr,scale_v_kpcGyr
  real(dp)::scale_hexp_invGyr, scale_hexp_kmsMpc

  ! one- and three-dimensional numbers of dtfe grid cells to average
  ! over
  integer :: n_av, n_av1, n_av2, n_av3
  real(dp) :: n_av_cubed ! TODO: modularise out of here?

  ! Turn this on for testing only.
  ! #define EDS_DUMMY_AEFF 1

#ifdef EDS_DUMMY_AEFF
  real(dp) :: t_zero_Gyr_EdS ! FLRW conventional t_0 (EdS)
  real(dp) :: dt_RZA
  integer i_t_RZA
#endif

#ifdef SCALAV_ALLOW512CUBED
  !TODO: Document the bug properly and post it to bitbucket;
  !and/or solve the bug.
  if(levelmin .ge. 9)then
     write(6,'(a,i4,a,i10,a)')'WARNING: levelmin = ',levelmin, &
          'but simulations of ',2**levelmin, &
          'or greater are likely to be WRONG. You have been warned.'
  endif
#endif

#ifndef WITHOUTMPI
  call MPI_COMM_RANK(MPI_COMM_WORLD, i_mpi_rank, ierr_scalav)
  call MPI_BARRIER(MPI_COMM_WORLD,ierr_scalav)

  ! as in output_amr.f90
  npart_loc = npart

  call MPI_ALLREDUCE(npart_loc,npart_tot,one, RAMMPI_I8B_TYPE, MPI_SUM, MPI_COMM_WORLD, ierr_scalav)

  allocate(xp_local(npart_tot,3)) ! TODO: reduce wasted memory
  allocate(vp_local(npart_tot,3))
  allocate(mp_local(npart_tot))
  allocate(levelp_local(npart_tot)) ! for double-checking only
#else
  npart_tot = npart ! non-MPI case: only one local cpu
#endif /* WITHOUTMPI */

  ! treate cpu 0 as the main cpu for collecting particle data
#ifndef WITHOUTMPI
  if(0 == i_mpi_rank)then
     write(*,'(a,i12,a,i12,a)')'MPI_ALLREDUCE gives: npart_tot = ',npart_tot,' (rank = ',i_mpi_rank,')'
#else
     write(*,'(a,i12)')'npart_tot = ',npart_tot
#endif /* WITHOUTMPI */

     allocate(xp_global(npart_tot,3))
     allocate(vp_global(npart_tot,3))
#ifdef SCALAV_CHECK_V_SCALE
     if(i_time_step .eq. 0)then
        allocate(xp_global_old(npart_tot,3))
        i_time_step = 1
     endif
#endif
     allocate(mp_global(npart_tot))
     allocate(levelp_global(npart_tot))

     ! store xp, vp, mp, levelp data from rank 0 worker in *_global arrays

     do i_dim=1,ndim
        npart_cumul = 0
        do i_part=1,npartmax
           if(levelp(i_part) > 0) then
              npart_cumul = npart_cumul +1
              xp_global(npart_cumul,i_dim) = xp(i_part,i_dim)
              vp_global(npart_cumul,i_dim) = vp(i_part,i_dim)
              if(1==i_dim)then
                 mp_global(npart_cumul) = mp(i_part)
                 levelp_global(npart_cumul) = levelp(i_part)
              endif
           endif
        enddo
     enddo


#ifndef WITHOUTMPI
     ! wait to receive  xp, vp, mp, levelp data from other cpus
     do i_mpi_rank_other = one,ncpu-one
        ! TODO: Portability WARNING: npart in pm/pm_parameters.f90 and
        ! npart_partial in this subroutine are declared as "i8b"
        ! integers. Checks against inconsistent types or portability
        ! improvements are needed. This should be tidied up more
        ! widely.
        call MPI_RECV(npart_partial, one, RAMMPI_I8B_TYPE, &
             i_mpi_rank_other, tag_npart, &
             MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierr_scalav)

        do i_dim=1,int(ndim,i4b)
           call MPI_RECV(xp_global(npart_cumul+1,i_dim), int(npart_partial,i4b), MPI_REAL8, &
                i_mpi_rank_other, tag_xp+i_dim, &
                MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierr_scalav)
           call MPI_RECV(vp_global(npart_cumul+1,i_dim), int(npart_partial,i4b), MPI_REAL8, &
                i_mpi_rank_other, tag_vp+i_dim, &
                MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierr_scalav)
        enddo

        call MPI_RECV(mp_global(npart_cumul+1), int(npart_partial,i4b), MPI_REAL8, &
             i_mpi_rank_other, tag_mp, &
             MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierr_scalav)

        call MPI_RECV(levelp_global(npart_cumul+1), int(npart_partial,i4b), RAMMPI_DEFAULT_INT_TYPE, &
             i_mpi_rank_other, tag_levelp, &
             MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierr_scalav)
        npart_cumul = npart_cumul + npart_partial ! update total number so far
     enddo

  else
     ! In any cpu other than 0, send xp, vp, mp, levelp data to the 0 cpu.
     ! See above portability warning near MPI_RECV(npart_partial, ...).
     call MPI_SEND(npart, one, RAMMPI_I8B_TYPE, &
          zero, tag_npart, &
          MPI_COMM_WORLD, ierr_scalav)

     i_part_valid = 0
     do i_part=1,npartmax
        if(levelp(i_part) > 0)then
           i_part_valid = i_part_valid + 1
           do i_dim=1,ndim
              xp_local(i_part_valid,i_dim) = xp(i_part,i_dim)
           enddo
           do i_dim=1,ndim
              vp_local(i_part_valid,i_dim) = vp(i_part,i_dim)
           enddo
           mp_local(i_part_valid) = mp(i_part)
           levelp_local(i_part_valid) = levelp(i_part)
        endif
     enddo
     if(i_part_valid .ne. npart) &
          write(*,'(a,i12,a,i12,a)')'Warning: i_part_valid = ',i_part_valid, &
          ' .ne. ',npart,' = npart'

     if(int(int(npart,i4b),i8b).ne.npart)then
        write(*,'(a,i12,a,i12,a,i6)')'ERROR: int(npart,i4b)= ',int(npart,i4b), &
             ' .ne. ',npart, '; i_mpi_rank =',i_mpi_rank
        write(*,'(a)')'Either use MPI with 64-bit integer counts or rewrite amr_dtfe_interface.'
        call clean_stop
     endif

     do i_dim=1,ndim
        ! make the tags unique (for this time step) by adding i_dim
        call MPI_SEND(xp_local(1,i_dim), int(npart,i4b), MPI_REAL8, &
             zero, tag_xp+i_dim,  MPI_COMM_WORLD, ierr_scalav)
        call MPI_SEND(vp_local(1,i_dim), int(npart,i4b), MPI_REAL8, &
             zero, tag_vp+i_dim,  MPI_COMM_WORLD, ierr_scalav)
     enddo
     call MPI_SEND(mp_local, int(npart,i4b), MPI_REAL8, &
          zero, tag_mp,  MPI_COMM_WORLD, ierr_scalav)

     call MPI_SEND(levelp_local, int(npart,i4b), RAMMPI_DEFAULT_INT_TYPE, &
          zero, tag_levelp,  MPI_COMM_WORLD, ierr_scalav)

  endif !      if(i_mpi_rank == 0)then
#endif /* WITHOUTMPI */

#ifndef WITHOUTMPI
  ! TO CHECK: Would removing this improve speed non-trivially?
  call MPI_BARRIER(MPI_COMM_WORLD,ierr_scalav)
#endif /* WITHOUTMPI */

#ifndef WITHOUTMPI
  root_cpu_call_to_dtfe: if(0 == i_mpi_rank)then
#endif /* WITHOUTMPI */

     allocate(dtfe_velocity( &
          dtfe_n_gridsize, dtfe_n_gridsize, dtfe_n_gridsize, ndim))
     allocate(divergence( &
          dtfe_n_gridsize, dtfe_n_gridsize, dtfe_n_gridsize))
     allocate(IIinv( &
          dtfe_n_gridsize, dtfe_n_gridsize, dtfe_n_gridsize))
     allocate(IIIinv( &
          dtfe_n_gridsize, dtfe_n_gridsize, dtfe_n_gridsize))
     allocate(Q_kin_backreaction( &
          dtfe_n_gridsize, dtfe_n_gridsize, dtfe_n_gridsize))
     allocate(Q_sigma( &
          dtfe_n_gridsize, dtfe_n_gridsize, dtfe_n_gridsize))
     allocate(shear( &
          dtfe_n_gridsize, dtfe_n_gridsize, dtfe_n_gridsize))
     allocate(vorticity( &
          dtfe_n_gridsize, dtfe_n_gridsize, dtfe_n_gridsize))
     allocate(dtfe_density( &
          dtfe_n_gridsize, dtfe_n_gridsize, dtfe_n_gridsize))

     allocate(Omega_m_dtfe( &
          dtfe_n_gridsize, dtfe_n_gridsize, dtfe_n_gridsize))
     allocate(Omega_Q_dtfe( &
          dtfe_n_gridsize, dtfe_n_gridsize, dtfe_n_gridsize))
     allocate(Omega_R_dtfe( &
          dtfe_n_gridsize, dtfe_n_gridsize, dtfe_n_gridsize))
     allocate(vol_curved_dtfe( &
          dtfe_n_gridsize, dtfe_n_gridsize, dtfe_n_gridsize))


     ! scalar averaged
     if(scal_av_biscale)then
        if(.not.Raychaudhuri_nbody)then
           write(6,'(a)')'Biscale averaging for RZA case not yet implemented.'
           call clean_stop
        endif
        scal_av_n_gridsize1 = 2 ! 1 = overdense, 2 = underdense
        scal_av_n_gridsize2 = 1
        scal_av_n_gridsize3 = 1
     else
        scal_av_n_gridsize1 = scal_av_n_gridsize
        scal_av_n_gridsize2 = scal_av_n_gridsize
        scal_av_n_gridsize3 = scal_av_n_gridsize
     endif

     if(RZA_no_nbody)then
        allocate(aD_norm( &
             scal_av_n_gridsize1, scal_av_n_gridsize2, scal_av_n_gridsize3))
     endif

     allocate(divergence_av( &
          scal_av_n_gridsize1, scal_av_n_gridsize2, scal_av_n_gridsize3))
     allocate(divergence_sq_av( &
          scal_av_n_gridsize1, scal_av_n_gridsize2, scal_av_n_gridsize3))
     allocate(IIinv_av( &
          scal_av_n_gridsize1, scal_av_n_gridsize2, scal_av_n_gridsize3))
     allocate(IIIinv_av( &
          scal_av_n_gridsize1, scal_av_n_gridsize2, scal_av_n_gridsize3))
     allocate(shear_av( &
          scal_av_n_gridsize1, scal_av_n_gridsize2, scal_av_n_gridsize3))
     allocate(vorticity_av( &
          scal_av_n_gridsize1, scal_av_n_gridsize2, scal_av_n_gridsize3))
     allocate(dtfe_density_av( &
          scal_av_n_gridsize1, scal_av_n_gridsize2, scal_av_n_gridsize3))
     allocate(Eucl_vol_tot( &
          scal_av_n_gridsize1, scal_av_n_gridsize2, scal_av_n_gridsize3))


     ! This section is for double-checking the velocity units.
     ! Enable it on ONLY for small values of *_n_gridsize!
#ifdef SCALAV_CHECK_V_SCALE
    call units_kpc_Gyr(scale_l_kpc,scale_t_Gyr,scale_v_kpcGyr, &
         scale_hexp_invGyr, scale_hexp_kmsMpc)
    t_FLRW(i_time_step) = texp*scale_t_Gyr

    if(i_time_step .eq. 2)then
       ! The old time step should now be available.
       dt_FLRW = t_FLRW(2) - t_FLRW(1)
       ! Check if delta(x) / peculiar_velocity \approx delta(t).
       ipart_OK = 0
       dt_recalc = 0d0
       dt_recalc_sig = 0d0
       do i_part = 1, npart_tot
          do i_dim=1,ndim
             xx = xp_global(i_part,i_dim) - xp_global_old(i_part,i_dim)
             ! Assume T^3 model.
             if(xx.gt.0.5d0)then
                xx = boxsize - xx
             elseif(xx.lt.-0.5d0)then
                xx = xx + boxsize
             endif
             if(abs(vp_global(i_part,i_dim)).gt.1d-20)then
                vv = xx / vp_global(i_part,i_dim)
                dt_recalc = dt_recalc + vv
                dt_recalc_sig = dt_recalc_sig + vv*vv
                ipart_OK = ipart_OK + 1
             endif
          enddo
       enddo

       ! mean, st.dev.
       dt_recalc = dt_recalc / (dble(ipart_OK))
       if(ipart_OK.gt.1)then
          dt_recalc_sig = dt_recalc_sig/(dble(ipart_OK)) &
               - dt_recalc*dt_recalc
          if(dt_recalc_sig .gt. 1d-20)then
             dt_recalc_sig = sqrt(dt_recalc_sig) * &
                  sqrt(ipart_OK/(dble(ipart_OK)-1d0))
          else
             write(6,'(a,g13.5)')'Warning: dt_recalc_sig^2= ',dt_recalc_sig
          endif
       else
          write(6,'(a,i10,a,g13.5)')"Warning: ipart_OK = ",ipart_OK, &
               'dt_recalc_sig^2 sum = ',dt_recalc_sig
       endif
       dt_recalc = dt_recalc * scale_l_kpc / scale_v_kpcGyr
       dt_recalc_sig = dt_recalc_sig * scale_l_kpc / scale_v_kpcGyr

       write(6,'(a,i10)')'ipart_OK = ',ipart_OK
       write(6,'(a,g12.5,a,g12.5,a,g12.5)') &
            'dt_FLRW = ',dt_FLRW, &
            ' dt_recalc = ', dt_recalc, &
            ' dt_recalc_sig = ', dt_recalc_sig
    endif

    if(is_final_time())then
       deallocate(xp_global_old)
    else
       ! shift new info to old slot
       xp_global_old = xp_global
       if(i_time_step.eq.2)then
          t_FLRW(1) = t_FLRW(2)
       endif
       i_time_step = 2 ! from now on, always at second step
    endif
#endif /* SCALAV_CHECK_V_SCALE */

    call DTFE_f90_wrapper(npart_tot, &
          boxsize, dtfe_averaging_method, dtfe_n_gridsize, &
          xp_global, vp_global, mp_global, &
          dtfe_velocity, &
          divergence, IIinv, IIIinv, &
          Q_kin_backreaction, Q_sigma, &
          shear, vorticity, dtfe_density)

    write(*,'(a)')'amr_dtfe_interface: returned from DTFE_f90_wrapper.'

    if(npart_tot .ne. npart_cumul) &
         write(*,'(a,i12,a,i12)')'amr_dtfe_interface Warning: &
         & npart_tot = ',npart_tot, ' .ne. ',npart_cumul

     call units_kpc_Gyr(scale_l_kpc,scale_t_Gyr,scale_v_kpcGyr, &
          scale_hexp_invGyr, scale_hexp_kmsMpc)
     convert_I_invGyr = scale_v_kpcGyr/ scale_l_kpc
     if(first_hexp)then
        first_hexp = .false.
        hexp_init_invGyr = hexp * scale_hexp_invGyr
     endif
     convert_I_dimless = convert_I_invGyr / hexp_init_invGyr

     ! normalise the densities
     mean_density = sum(dtfe_density) &
          /exp(3.0*log(real(dtfe_n_gridsize,dp)))
     write(6,'(a,f15.5)')'amr_dtfe_interface: mean_density = ', &
          mean_density
     ! TODO: Non-EdS cases need to be handled using
     ! \Omega_{\mathrm{m}}(t) via "omega_m" (i.e. \Omega_{m0}^{FLRW})
     do ix=1,dtfe_n_gridsize
        do iy=1,dtfe_n_gridsize
           do iz=1,dtfe_n_gridsize
              dtfe_density(ix,iy,iz) = dtfe_density(ix,iy,iz) / mean_density
           enddo
        enddo
     enddo

     ! Estimate Omega parameters in DTFE cells; this is primarily
     ! for correctly using curvature for volume weighting.
     if_cal_curv: if(scal_av_curv_D .and. have_old_H_D_hexp)then
        print*,'before curv calc: old_H_D_common, old_hexp_invGyr = ',old_H_D_common, old_hexp_invGyr
        H_sq_factor = (old_hexp_invGyr/old_H_D_common)**2
        minus_sixHD_sq_factor = -1.0_dp/(6.0_dp* (old_H_D_common)**2)
        do ix=1,dtfe_n_gridsize
           do iy=1,dtfe_n_gridsize
              do iz=1,dtfe_n_gridsize
                 ! super-EdS expansion gives sub-EdS Omega_m
                 Omega_m_dtfe(ix,iy,iz) = dtfe_density(ix,iy,iz) * &
                      H_sq_factor ! dimensionless factor
                 Omega_Q_dtfe(ix,iy,iz) = Q_kin_backreaction(ix,iy,iz) * &
                      minus_sixHD_sq_factor ! dimensions [T^2] factor
                 Omega_R_dtfe(ix,iy,iz) = 1.0_dp - &
                      Omega_m_dtfe(ix,iy,iz) - &
                      Omega_Q_dtfe(ix,iy,iz)

                 ! Calculate curved volume element
                 vol_flat_tmp = (boxsize /real(dtfe_n_gridsize,dp) &
                      * scale_l_kpc / 1.0d3)**3
                 if(Omega_R_dtfe(ix,iy,iz) .lt. -TOL_CURV)then
                    ! spherical (positive curvature):
                    R_curv_tmp = c_spacetime_convert_kms / &
                         kms_per_MpcGyr & ! convert to Mpc/Gyr
                         / old_H_D_common & ! divide by H_D
                         / sqrt(-Omega_R_dtfe(ix,iy,iz))
                    sign_RC = 1
                    vol_curved_dtfe(ix,iy,iz) = &
                         curved_volume_3D(vol_flat_tmp, R_curv_tmp, sign_RC)
                 elseif(Omega_R_dtfe(ix,iy,iz) .gt. TOL_CURV)then
                    ! hyperbolic (negative curvature):
                    R_curv_tmp = c_spacetime_convert_kms / &
                         kms_per_MpcGyr & ! convert to Mpc/Gyr
                         / old_H_D_common & ! divide by H_D
                         / sqrt(Omega_R_dtfe(ix,iy,iz))
                    sign_RC = -1
                    vol_curved_dtfe(ix,iy,iz) = &
                         curved_volume_3D(vol_flat_tmp, R_curv_tmp, sign_RC)
                 else
                    vol_curved_dtfe(ix,iy,iz) = vol_flat_tmp
                 endif
              enddo
           enddo
        enddo
     endif if_cal_curv


     ! Do scalar averaging.
     if_use_Lagrange : if((.not. RZA_no_nbody) .and. use_Lagrange)then
        ! if first step, then store Lagrangian initial-DTFE-cell identities
        dtfe_grid_per_box = real(dtfe_n_gridsize,dp) / boxsize
        scal_av_grid_per_box = real(scal_av_n_gridsize,dp) / boxsize
        if(.not. have_Lag_IDs)then
           allocate(ijk_Lagrangian(npart_tot,3))
           allocate(n_per_Lag_cell( &
                scal_av_n_gridsize1, scal_av_n_gridsize2, scal_av_n_gridsize3))

           do ix=1,scal_av_n_gridsize1
              do iy=1,scal_av_n_gridsize2
                 do iz=1,scal_av_n_gridsize3
                    n_per_Lag_cell(ix,iy,iz) = 0 ! TODO: f90 shorthand OK?
                 enddo
              enddo
           enddo

           do i_part=1,npart_tot
              interp_if_biscale: if(scal_av_biscale)then
                 do i_dim=1,ndim
                    xx = xp_global(i_part,i_dim) * dtfe_grid_per_box
                    !!! TODO: modularise 3D T^3 interpolation !!!
                    ! allow for T^3 topology of cosmological volume
                    ip_lo(i_dim) = mod(int(xx, i8b), dtfe_n_gridsize) +1
                    ip_hi(i_dim) = mod(int(xx, i8b)+1, dtfe_n_gridsize) +1
                    ! TODO: safety check against negative xp_global values !
                    frac_hi(i_dim) = xx - real(int(xx, i8b), dp)
                    frac_lo(i_dim) = 1.0_dp - frac_hi(i_dim)
                 enddo

                 ! linearly interpolate density, i.e. inverse volume (* constant)
                 density_interpolated = &
                      interpolate_Euclid_3D(dtfe_density, &
                      ip_lo, ip_hi, frac_lo, frac_hi)

                 if(density_interpolated .ge. 1d0)then
                    ijk_Lagrangian(i_part,1) = 1
                 else
                    ijk_Lagrangian(i_part,1) = 2
                 endif
                 !print*,'debug:density_interpolated =',density_interpolated, &
                  !    frac_lo(1),frac_lo(2),frac_lo(3), &
                 !frac_hi(1),frac_hi(2),frac_hi(3)
                 ijk_Lagrangian(i_part,2) = 1
                 ijk_Lagrangian(i_part,3) = 1
              else
                 do i_dim=1,ndim
                    xx = xp_global(i_part,i_dim) * scal_av_grid_per_box
                    select case (i_dim) ! TODO: scal_av_n_gridsize_i(i_dim) (throughout this function)
                    case (1)
                       ijk_Lagrangian(i_part,i_dim) = mod(int(xx, i8b), scal_av_n_gridsize1) +1
                    case (2)
                       ijk_Lagrangian(i_part,i_dim) = mod(int(xx, i8b), scal_av_n_gridsize2) +1
                    case (3)
                       ijk_Lagrangian(i_part,i_dim) = mod(int(xx, i8b), scal_av_n_gridsize3) +1
                    end select
                 enddo
              endif interp_if_biscale
              n_per_Lag_cell(ijk_Lagrangian(i_part,1), &
                   ijk_Lagrangian(i_part,2), &
                   ijk_Lagrangian(i_part,3)) = &
                   n_per_Lag_cell(ijk_Lagrangian(i_part,1), &
                   ijk_Lagrangian(i_part,2), &
                   ijk_Lagrangian(i_part,3)) +1
           enddo

           have_Lag_IDs = .true.
        endif
        do ix=1,scal_av_n_gridsize1
           do iy=1,scal_av_n_gridsize2
              do iz=1,scal_av_n_gridsize3
                 if( n_per_Lag_cell(ix,iy,iz) .lt. &
                      min_per_Lag_subdomain )then
                    min_per_Lag_subdomain = n_per_Lag_cell(ix,iy,iz)
                 endif
              enddo
           enddo
        enddo
        if(scal_av_biscale)then
           write(6,'(a,2i15)')'n_per_Lag_cell = ', &
                n_per_Lag_cell(1,1,1),n_per_Lag_cell(2,1,1)
        endif
        if( min_per_Lag_subdomain .le. 0)then

           write(6,'(a)')'use_Lagrange: Too few particles per initial time step'
           write(6,'(a)')' Lagrangian subdomain.'
           write(6,'(a)')' Either decrease DTFE resolution or increase particle resolution.'

           !call clean_stop
           stop ! clean_stop does not seem to kill the mpi threads
        endif


        ! Calculate volume-weighted (i.e. inverse-density-weighted) means.
        ! First initialise cumulators to zero:
        do ix=1,scal_av_n_gridsize1
           do iy=1,scal_av_n_gridsize2
              do iz=1,scal_av_n_gridsize3
                 divergence_av(ix,iy,iz) = 0.0_dp !TODO: test f90 shorthand
                 divergence_sq_av(ix,iy,iz) = 0.0_dp !TODO: test f90 shorthand
                 IIinv_av(ix,iy,iz) = 0.0_dp
                 IIIinv_av(ix,iy,iz) = 0.0_dp
                 shear_av(ix,iy,iz) = 0.0_dp
                 vorticity_av(ix,iy,iz) = 0.0_dp
                 Eucl_vol_tot(ix,iy,iz) = 0.0_dp
              enddo
           enddo
        enddo
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!BEGIN:TODO: modularise this interpolation! !!!!!!!

        sigma_omega_m_FLRW = 0d0 ! mass rms
        n_antiflow = 0 !

        ! Next, interpolate and cumulate the closest eight DTFE values.
        ipart_loop: do i_part=1,npart_tot
           ! fractional position
           do i_dim=1,ndim
              xx = xp_global(i_part,i_dim) * dtfe_grid_per_box
              ! allow for T^3 topology of cosmological volume
              ip_lo(i_dim) = mod(int(xx, i8b), dtfe_n_gridsize) +1
              ip_hi(i_dim) = mod(int(xx, i8b)+1, dtfe_n_gridsize) +1
              ! TODO: safety check against negative xp_global values !
              frac_hi(i_dim) = xx - real(int(xx, i8b), dp)
              frac_lo(i_dim) = 1.0_dp - frac_hi(i_dim)
           enddo


           vdtfe_dot_vpart = 0_dp
           vpart_mod = 0_dp
           do i_dim=1,ndim
              vpart_mod = vpart_mod + &
                   vp_global(i_part,i_dim) * &
                   vp_global(i_part,i_dim)
              vdtfe_interp = &
                   interpolate_Euclid_3D(dtfe_velocity(:,:,:,i_dim), &
                   ip_lo, ip_hi, frac_lo, frac_hi)

              vdtfe_dot_vpart = vdtfe_dot_vpart +  &
                   vp_global(i_part,i_dim) * vdtfe_interp
           enddo

           ! cumulators
           ! linearly interpolate density, i.e. inverse volume (* constant)
           inv_vol_tmp = &
                interpolate_Euclid_3D(dtfe_density, &
                ip_lo, ip_hi, frac_lo, frac_hi)

           ! convert to volume
           vol_tmp = 1.0_dp /max( TOL_VOL_AMR_DTFE, inv_vol_tmp )

           ! curved volume factor: override the flat volume element if possible
           if(scal_av_curv_D .and. have_old_H_D_hexp)then
              vol_tmp = vol_tmp * &
                   interpolate_Euclid_3D(vol_curved_dtfe, &
                   ip_lo, ip_hi, frac_lo, frac_hi)
           endif

           Eucl_vol_tot(ijk_Lagrangian(i_part,1), &
                ijk_Lagrangian(i_part,2), &
                ijk_Lagrangian(i_part,3)) = &
                Eucl_vol_tot(ijk_Lagrangian(i_part,1), &
                ijk_Lagrangian(i_part,2), &
                ijk_Lagrangian(i_part,3)) &
                + vol_tmp


           divergence_interp = &
                interpolate_Euclid_3D(divergence, &
                ip_lo, ip_hi, frac_lo, frac_hi)

           divergence_av(ijk_Lagrangian(i_part,1), &
                ijk_Lagrangian(i_part,2), &
                ijk_Lagrangian(i_part,3)) = &
                divergence_av(ijk_Lagrangian(i_part,1), &
                ijk_Lagrangian(i_part,2), &
                ijk_Lagrangian(i_part,3)) + &
                vol_tmp * & ! weighting
                ! linearly interpolate divergence
                divergence_interp

           divergence_sq_av(ijk_Lagrangian(i_part,1), &
                ijk_Lagrangian(i_part,2), &
                ijk_Lagrangian(i_part,3)) = &
                divergence_sq_av(ijk_Lagrangian(i_part,1), &
                ijk_Lagrangian(i_part,2), &
                ijk_Lagrangian(i_part,3)) + &
                vol_tmp * & ! weighting
                ! linearly interpolate divergence
                divergence_interp*divergence_interp

           ! This could be placed earlier; placing it here
           ! could be useful if a divergence condition were added.
           if(vdtfe_dot_vpart .lt. 0d0 .and. &
                vpart_mod .gt. (-vdtfe_dot_vpart) )then
              n_antiflow = n_antiflow +1
           endif


           IIinv_interp = &
                interpolate_Euclid_3D(IIinv, &
                ip_lo, ip_hi, frac_lo, frac_hi)

           IIinv_av(ijk_Lagrangian(i_part,1), &
                ijk_Lagrangian(i_part,2), &
                ijk_Lagrangian(i_part,3)) = &
                IIinv_av(ijk_Lagrangian(i_part,1), &
                ijk_Lagrangian(i_part,2), &
                ijk_Lagrangian(i_part,3)) + &
                vol_tmp * & ! weighting
                ! linearly interpolate IInd invariant
                IIinv_interp


           shear_interp = &
                interpolate_Euclid_3D(shear, &
                ip_lo, ip_hi, frac_lo, frac_hi)

           shear_av(ijk_Lagrangian(i_part,1), &
                ijk_Lagrangian(i_part,2), &
                ijk_Lagrangian(i_part,3)) = &
                shear_av(ijk_Lagrangian(i_part,1), &
                ijk_Lagrangian(i_part,2), &
                ijk_Lagrangian(i_part,3)) + &
                vol_tmp * & ! weighting
                ! linearly interpolate shear
                shear_interp

           vorticity_interp = &
                interpolate_Euclid_3D(vorticity, &
                ip_lo, ip_hi, frac_lo, frac_hi)

           vorticity_av(ijk_Lagrangian(i_part,1), &
                ijk_Lagrangian(i_part,2), &
                ijk_Lagrangian(i_part,3)) = &
                vorticity_av(ijk_Lagrangian(i_part,1), &
                ijk_Lagrangian(i_part,2), &
                ijk_Lagrangian(i_part,3)) + &
                vol_tmp * & ! weighting
                ! linearly interpolate vorticity
                vorticity_interp


           IIIinv_interp = &
                interpolate_Euclid_3D(IIIinv, &
                ip_lo, ip_hi, frac_lo, frac_hi)

           IIIinv_av(ijk_Lagrangian(i_part,1), &
                ijk_Lagrangian(i_part,2), &
                ijk_Lagrangian(i_part,3)) = &
                IIIinv_av(ijk_Lagrangian(i_part,1), &
                ijk_Lagrangian(i_part,2), &
                ijk_Lagrangian(i_part,3)) + &
                vol_tmp * & ! weighting
                ! linearly interpolate IIIrd invariant
                IIIinv_interp

        enddo ipart_loop

        f_antiflow = real(n_antiflow,dp)/real(npart_tot,dp)
        !write(6,'(a,f15.5)')'f_antiflow= ',f_antiflow

        ! calculate vol-weighted means
        do ix=1,scal_av_n_gridsize1
           do iy=1,scal_av_n_gridsize2
              do iz=1,scal_av_n_gridsize3
                 if( Eucl_vol_tot(ix,iy,iz) .lt. TOL_VOL_AMR_DTFE)then
                    Eucl_vol_tot(ix,iy,iz) = TOL_VOL_AMR_DTFE
                    if(i_warn_zero_vol .lt. max_warn_zero_vol)then
                       i_warn_zero_vol = i_warn_zero_vol + 1
                       write(*,'(a)')'use_Lagrange: WARNING: Zero volume cell!'
                    endif
                 endif
                 divergence_av(ix,iy,iz) = &
                      divergence_av(ix,iy,iz) / &
                      Eucl_vol_tot(ix,iy,iz)

                 divergence_sq_av(ix,iy,iz) = &
                      divergence_sq_av(ix,iy,iz) / &
                      Eucl_vol_tot(ix,iy,iz)

                 IIinv_av(ix,iy,iz) = &
                      IIinv_av(ix,iy,iz) / &
                      Eucl_vol_tot(ix,iy,iz)

                 shear_av(ix,iy,iz) = &
                      shear_av(ix,iy,iz) / &
                      Eucl_vol_tot(ix,iy,iz)

                 vorticity_av(ix,iy,iz) = &
                      vorticity_av(ix,iy,iz) / &
                      Eucl_vol_tot(ix,iy,iz)

                 IIIinv_av(ix,iy,iz) = &
                      IIIinv_av(ix,iy,iz) / &
                      Eucl_vol_tot(ix,iy,iz)

                 dtfe_density_av(ix,iy,iz) = &
                      real(n_per_Lag_cell(ix,iy,iz),dp) / &
                      Eucl_vol_tot(ix,iy,iz)
                 ! assume EdS and assume a_EDS = a_eff
                 sigma_omega_m_FLRW = sigma_omega_m_FLRW + &
                      (dtfe_density_av(ix,iy,iz) - 1d0)**2

              enddo
           enddo
        enddo


        sigma_omega_m_FLRW = sqrt(sigma_omega_m_FLRW/ &
             (scal_av_n_gridsize1* scal_av_n_gridsize2* &
             scal_av_n_gridsize3))
        write(*,'(a,2e15.5)')'use_Lagrange==.true.:sigma_omega_m_FLRW at DTFE cell scale = ', &
                sigma_omega_m_FLRW, &
                sigma_omega_m_FLRW/aexp

!!!!!!!!END:TODO: modularise this interpolation! !!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

     else ! this branch is the non-use_Lagrange case
        sigma_omega_m_FLRW = 0d0 ! mass rms
        ! TODO: Warn of fail if dtfe_n_gridsize and scal_av_n_gridsize
        ! are incompatible.
        n_av = dtfe_n_gridsize/scal_av_n_gridsize ! integer arithmetic
        n_av1 = dtfe_n_gridsize/scal_av_n_gridsize1 ! integer arithmetic ! TODO: add similar check
        n_av2 = dtfe_n_gridsize/scal_av_n_gridsize2 ! integer arithmetic ! TODO: add similar check
        n_av3 = dtfe_n_gridsize/scal_av_n_gridsize3 ! integer arithmetic ! TODO: add similar check
        n_av_cubed = real(n_av1,dp) *real(n_av2,dp) * real(n_av3,dp)
        do iz=1,scal_av_n_gridsize3
           do iy=1,scal_av_n_gridsize2
              do ix=1,scal_av_n_gridsize1
                 !modern fortran sum() version
                 !divergence_av(ix,iy,iz) = &
                 !     sum(divergence(n_av*(ix-1)+1:n_av*ix, &
                 !     n_av*(iy-1)+1:n_av*iy, &
                 !     n_av*(iz-1)+1:n_av*iz)) /n_av_cubed

                 !f77 style loop version
                 divergence_av(ix,iy,iz) = 0.0_dp
                 do ixx = n_av*(ix-1)+1, n_av*ix
                    do iyy = n_av*(iy-1)+1, n_av*iy
                       do izz = n_av*(iz-1)+1, n_av*iz
                          divergence_av(ix,iy,iz) = &
                               divergence_av(ix,iy,iz) + &
                               divergence(ixx,iyy,izz)
                       enddo
                    enddo
                 enddo
                 divergence_av(ix,iy,iz) =  divergence_av(ix,iy,iz) &
                      /n_av_cubed


                 ! TODO Is there an f90 shortcut to this without a slowdown?
                 divergence_sq_av(ix,iy,iz) = 0.0_dp
                 do ixx = n_av*(ix-1)+1, n_av*ix
                    do iyy = n_av*(iy-1)+1, n_av*iy
                       do izz = n_av*(iz-1)+1, n_av*iz
                          divergence_sq_av(ix,iy,iz) = &
                               divergence_sq_av(ix,iy,iz) + &
                               divergence(ixx,iyy,izz) * &
                               divergence(ixx,iyy,izz)
                       enddo
                    enddo
                 enddo
                 divergence_sq_av(ix,iy,iz) =  divergence_sq_av(ix,iy,iz) &
                      /n_av_cubed

                 IIinv_av(ix,iy,iz) = &
                      sum(IIinv(n_av*(ix-1)+1:n_av*ix, &
                      n_av*(iy-1)+1:n_av*iy, &
                      n_av*(iz-1)+1:n_av*iz)) /n_av_cubed

                 shear_av(ix,iy,iz) = &
                      sum(shear(n_av*(ix-1)+1:n_av*ix, &
                      n_av*(iy-1)+1:n_av*iy, &
                      n_av*(iz-1)+1:n_av*iz)) /n_av_cubed

                 vorticity_av(ix,iy,iz) = &
                      sum(vorticity(n_av*(ix-1)+1:n_av*ix, &
                      n_av*(iy-1)+1:n_av*iy, &
                      n_av*(iz-1)+1:n_av*iz)) /n_av_cubed

                 IIIinv_av(ix,iy,iz) = &
                      sum(IIIinv(n_av*(ix-1)+1:n_av*ix, &
                      n_av*(iy-1)+1:n_av*iy, &
                      n_av*(iz-1)+1:n_av*iz)) /n_av_cubed

                 dtfe_density_av(ix,iy,iz) = &
                      sum(dtfe_density(n_av*(ix-1)+1:n_av*ix, &
                      n_av*(iy-1)+1:n_av*iy, &
                      n_av*(iz-1)+1:n_av*iz)) /n_av_cubed
                 ! assume EdS and assume a_EdS = a_eff:
                 sigma_omega_m_FLRW = sigma_omega_m_FLRW + &
                      (dtfe_density_av(ix,iy,iz) - 1d0)**2
              enddo
           enddo
        enddo
        sigma_omega_m_FLRW = sqrt(sigma_omega_m_FLRW/ &
             (scal_av_n_gridsize1 * scal_av_n_gridsize2 &
             * scal_av_n_gridsize3))
        write(*,'(a,e15.5)')'use_Lagrange==.false.:sigma_omega_m_FLRW at DTFE cell scale = ', &
             sigma_omega_m_FLRW
     endif if_use_Lagrange


     ! set units of I, II, III
        do ix=1,scal_av_n_gridsize1
           do iy=1,scal_av_n_gridsize2
              do iz=1,scal_av_n_gridsize3
                 if(RZA_no_nbody)then
                    divergence_av(ix,iy,iz) = divergence_av(ix,iy,iz)* &
                         convert_I_dimless
                    divergence_sq_av(ix,iy,iz) = divergence_sq_av(ix,iy,iz)* &
                         convert_I_dimless**2
                    IIinv_av(ix,iy,iz) = IIinv_av(ix,iy,iz)* &
                         convert_I_dimless**2
                    shear_av(ix,iy,iz) = shear_av(ix,iy,iz)* &
                         convert_I_dimless**2
                    vorticity_av(ix,iy,iz) = vorticity_av(ix,iy,iz)* &
                         convert_I_dimless**2
                    IIIinv_av(ix,iy,iz) = IIIinv_av(ix,iy,iz)* &
                         convert_I_dimless**3
                 else
                    divergence_av(ix,iy,iz) = divergence_av(ix,iy,iz)* &
                         convert_I_invGyr
                    divergence_sq_av(ix,iy,iz) = divergence_sq_av(ix,iy,iz)* &
                         convert_I_invGyr**2
                    IIinv_av(ix,iy,iz) = IIinv_av(ix,iy,iz)* &
                         convert_I_invGyr**2
                    shear_av(ix,iy,iz) = shear_av(ix,iy,iz)* &
                         convert_I_invGyr**2
                    vorticity_av(ix,iy,iz) = vorticity_av(ix,iy,iz)* &
                         convert_I_invGyr**2
                    IIIinv_av(ix,iy,iz) = IIIinv_av(ix,iy,iz)* &
                         convert_I_invGyr**3
                 endif
              enddo
           enddo
        enddo



#ifdef SCALAV_CHECK_AMR_DTFE_INTERFACE
     ! sanity checks - check some statistics of the particles
     call amr_dtfe_interface_sanity_check(xp_global, &
          vp_global, mp_global, levelp_global, npart_tot, &
          Q_kin_backreaction, dtfe_density)
#endif

#ifndef WITHOUTMPI
  endif root_cpu_call_to_dtfe
#endif /* WITHOUTMPI */

#ifndef WITHOUTMPI
  if(0 == i_mpi_rank)then
#endif /* WITHOUTMPI */

     allocate(t_RZA(n_t_inhomog))
     allocate(a_D_RZA(n_t_inhomog))

     ! test functionality without calling Omega_D_precalc in inhomog
#ifdef EDS_DUMMY_AEFF
     t_zero_Gyr_EdS = 2.0d0/(3.0d0* h0) *kms_per_MpcGyr
     dt_RZA = t_zero_Gyr/dble(n_t_inhomog)
     ! Warning: Assume that n_t_inhomog is big enough for the first
     ! time step to be earlier than the simulation start time.
     ! For test purposes only!
     do i_t_RZA = 1, n_t_inhomog
        t_RZA(i_t_RZA) = dble(i_t_RZA) * dt_RZA
        a_D_RZA(i_t_RZA) = exp( (2.0d0/3.0d0)* &
             log( t_RZA(i_t_RZA)/t_zero_Gyr ) ) ! a_{EdS} = (t/t0)^{2/3}
        write(*,'(a,2f12.5)')'TEST:a_D^EdS= ',t_RZA(i_t_RZA), a_D_RZA(i_t_RZA)
     enddo
#endif

     ! RZA_no_nbody and Raychaudhuri_nbody are mutually exclusive
     if(RZA_no_nbody)then
        if(Raychaudhuri_nbody)then
           write(6,'(a)')'amr_dtfe_interface: WARNING: Raychaudhuri_nbody value ignored'
           write(6,'(a)')'because RZA_no_nbody enabled.'
        endif

        ! domain-wise scale factor initial normalisation
        do iz=1,scal_av_n_gridsize3
           do iy=1,scal_av_n_gridsize2
              do ix=1,scal_av_n_gridsize1
                 aD_norm(ix,iy,iz) = 1.0_dp
              enddo
           enddo
        enddo

        write(6,'(a,i20)')'amr_dtfe_interface: n_t_inhomog = ',n_t_inhomog
        call Omega_D_precalc( &! INPUTS
             scal_av_n_gridsize1, &
             scal_av_n_gridsize2, &
             scal_av_n_gridsize3, &
             divergence_av, IIinv_av, IIIinv_av, &
             aD_norm, &
             n_t_inhomog, &
             aexp, &
             h0, &
             ! OUTPUTS
             t_RZA, a_D_RZA &
             ) !bind (c)

        !2020-05-08: a clean_stop is called below with the statement
        ! `if(.not.Raychaudhuri_nbody)`

        !stop !call clean_stop ! "no_nbody" should mean literally no
        !N-body calculations
     elseif(Raychaudhuri_nbody)then
        call update_av_scalars_global(&
             divergence_av, &
             divergence_sq_av, &
             IIinv_av, &
             shear_av, &
             vorticity_av, &
             dtfe_density_av, &
             Eucl_vol_tot, &
             f_antiflow, &
             is_final_time() & ! last_step
             )

        if(have_H_D_estimate)then
           ! save FLRW and effective expansion rates for Omega estimates
           old_H_D_common = H_D_common ! inv Julian Gyr
           old_hexp_invGyr = hexp_invGyr ! inv Julian Gyr
           have_old_H_D_hexp = .true.
           print*,'After update_av: old_H_D_common, old_hexp_invGyr = ',old_H_D_common, old_hexp_invGyr
        endif
     endif

     ! TODO: coordinate with update_time.f90 routines to set last_step = .true.
     ! at the final time step; e.g. subroutine clean_stop  and
     ! "     if(t>=tout(noutput).or.aexp>=aout(noutput).or. & " ...

     ! TODO (memory): These should in principle be redundant due to
     ! automatic dellocation; alternatively declare them in a higher
     ! scope so that they can be allocated at the first time step, and
     ! deallocated at the final step.

     deallocate(a_D_RZA)
     deallocate(t_RZA)

     deallocate(Eucl_vol_tot)
     deallocate(vorticity_av)
     deallocate(shear_av)
     deallocate(dtfe_density_av)
     deallocate(IIIinv_av)
     deallocate(IIinv_av)
     deallocate(divergence_sq_av)
     deallocate(divergence_av)

     deallocate(vol_curved_dtfe)
     deallocate(Omega_R_dtfe)
     deallocate(Omega_Q_dtfe)
     deallocate(Omega_m_dtfe)
     deallocate(dtfe_density)
     deallocate(vorticity)
     deallocate(shear)
     deallocate(Q_sigma)
     deallocate(Q_kin_backreaction)
     deallocate(IIIinv)
     deallocate(IIinv)
     deallocate(divergence)

     deallocate(levelp_global)
     deallocate(mp_global)
     deallocate(vp_global)
     deallocate(xp_global)

     if(use_Lagrange)then
        if(is_final_time())then
           ! deallocate(ijk_Lagrangian) ! TODO: enable this after checking!
           ! deallocate(n_per_Lag_cell)
        endif
     elseif(RZA_no_nbody)then
        if(is_final_time())then
           deallocate(aD_norm)
        endif
     endif

#ifndef WITHOUTMPI
     deallocate(levelp_local)
     deallocate(mp_local)
     deallocate(vp_local)
     deallocate(xp_local)
  endif
#endif /* WITHOUTMPI */

#ifndef WITHOUTMPI
  ! Send the revised expansion rate parameters to non-primary cpus;
  ! they will not be allowed to continue until receiving these values.
  call MPI_BCAST(a_D_common,one,MPI_DOUBLE_PRECISION,zero,MPI_COMM_WORLD,ierr_scalav)
  call MPI_BCAST(H_D_common,one,MPI_DOUBLE_PRECISION,zero,MPI_COMM_WORLD,ierr_scalav)
  call MPI_BCAST(have_a_D_estimate,one,MPI_LOGICAL,zero,MPI_COMM_WORLD,ierr_scalav)
#endif /* WITHOUTMPI */

  ! Calculating a_D_RZA is an interesting enough output; avoid
  ! running the main (Raychaudhuri integrator) part of the N-body
  ! simulation.
  if(.not.Raychaudhuri_nbody)call clean_stop


contains

  ! Given a field on a grid and indices and fractions within a cell of
  ! the grid, linearly interpolate the field value.
#define INTERP_SANITY_CHECK 1
#ifdef INTERP_SANITY_CHECK
  function interpolate_Euclid_3D(field, ip_lo, ip_hi, &
       frac_lo, frac_hi)
#else
  pure function interpolate_Euclid_3D(field, ip_lo, ip_hi, &
       frac_lo, frac_hi)
#endif
    use amr_commons, only : dp, i8b
    use amr_parameters, only : ndim

    implicit none

    ! INPUTS:
    real(dp), intent(in), dimension(:,:,:) :: field
    integer(i8b), intent(in) :: ip_lo(3) ! lower dtfe cell corner ID
    integer(i8b), intent(in) :: ip_hi(3) ! upper dtfe cell corner ID
    real(dp), intent(in) :: frac_lo(3) ! fraction of dtfe cell in i_dim-th direction
    real(dp), intent(in) :: frac_hi(3) ! 1 - frac_lo(.)

    ! OUTPUTS:
    real(dp) :: interpolate_Euclid_3D

#ifdef INTERP_SANITY_CHECK
#define INTERP_SANITY_CHECK_MAX 1000
#define TOL_FRAC_ZERO 1e-10
    ! LOCAL:
    integer, save :: i_sanity_check = 0! full check would be too slow
    integer :: i_dim
#endif

#ifdef INTERP_SANITY_CHECK
    if(i_sanity_check .lt. INTERP_SANITY_CHECK_MAX)then
       do i_dim=1,ndim
          if(ip_lo(i_dim) .lt. lbound(field,i_dim) .or. &
               ip_hi(i_dim) .gt. ubound(field,i_dim) .or. &
               frac_lo(i_dim) .lt. -TOL_FRAC_ZERO .or. &
               frac_hi(i_dim) .gt. 1+TOL_FRAC_ZERO)then
             write(6,'(a,4i12,2es16.7)')'interpolate_Euclid_3D: out-of-bounds WARNING ', &
                  ip_lo(i_dim), lbound(field,i_dim), &
                  ip_hi(i_dim), ubound(field,i_dim), &
                  frac_lo(i_dim), &
                  frac_hi(i_dim)
          endif
       enddo
       i_sanity_check = i_sanity_check + 1
    endif
#endif

    interpolate_Euclid_3D = &
         frac_lo(1)*frac_lo(2)*frac_lo(3)* &
         field(ip_lo(1),ip_lo(2),ip_lo(3)) &
         + &
         frac_lo(1)*frac_lo(2)*frac_hi(3)* &
         field(ip_lo(1),ip_lo(2),ip_hi(3)) &
         + &
         frac_lo(1)*frac_hi(2)*frac_lo(3)* &
         field(ip_lo(1),ip_hi(2),ip_lo(3)) &
         + &
         frac_lo(1)*frac_hi(2)*frac_hi(3)* &
         field(ip_lo(1),ip_hi(2),ip_hi(3)) &
         + &
         frac_hi(1)*frac_lo(2)*frac_lo(3)* &
         field(ip_hi(1),ip_lo(2),ip_lo(3)) &
         + &
         frac_hi(1)*frac_lo(2)*frac_hi(3)* &
         field(ip_hi(1),ip_lo(2),ip_hi(3)) &
         + &
         frac_hi(1)*frac_hi(2)*frac_lo(3)* &
         field(ip_hi(1),ip_hi(2),ip_lo(3)) &
         + &
         frac_hi(1)*frac_hi(2)*frac_hi(3)* &
         field(ip_hi(1),ip_hi(2),ip_hi(3))

  end function interpolate_Euclid_3D


#ifdef SCALAV_CHECK_AMR_DTFE_INTERFACE
  ! sanity checks for development mode of scalar averaging DTFE interface
  subroutine amr_dtfe_interface_sanity_check(xp_global, &
       vp_global, mp_global, levelp_global, npart_tot, &
       Q_kin_backreaction, dtfe_density)
    use amr_commons
    use pm_commons
    implicit none

    ! global for full simulation at a given time step
    real(dp), dimension(:,:), intent(in) ::xp_global   ! Positions
    real(dp), dimension(:,:), intent(in) ::vp_global   ! Velocities
    real(dp), dimension(:), intent(in) ::mp_global   ! Masses
    integer, dimension(:), intent(in) :: levelp_global ! Current particle level

    real(dp), intent(in), allocatable, dimension(:,:,:) :: Q_kin_backreaction
    real(dp), intent(in), allocatable, dimension(:,:,:) :: dtfe_density

    integer(i8b), intent(in) :: npart_tot

    ! total mass and position/velocity gaussian statistics
    real(dp) :: mtotal, xpmean, vpmean, xpsigma, vpsigma

    real(dp) :: Q_mean, Q_sigma
    real(dp) :: rho_mean, rho_sigma

    ! Unit conversion factors - see amr/units.f90
    real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
    real(dp)::scale_l_kpc,scale_t_Gyr,scale_v_kpcGyr
    real(dp)::scale_hexp_invGyr, scale_hexp_kmsMpc

    ! 1 kpc in terms of cm
    !real(dp) :: Mpc_cm = 3.086e24
    !real(dp) :: boxsize_Mpc, pecvel_unit_kms
    real(dp) :: convert_Q ! simulation units -> 1/Gyr^2 (CHECK!)

    integer :: i_dim
    integer :: i_part ! particle iterator - max value
    integer :: i_part_valid ! particle iterator - valid particles
    integer :: ip,jp,kp

#ifdef __GNUC__
    if(sizeof(levelp)/npartmax .ne. sizeof(levelp_global)/npart_tot)then
       write(*,'(a,i12)')'amr_dtfe_interface_sanity_check: &
            &Warning: sizeof(levelp)/npartmax = ',sizeof(levelp)/npartmax
       write(*,'(a,i12)')'but sizeof(levelp_global)/npart_tot = ',sizeof(levelp_global)/npart_tot
    endif
#endif

    i_part_valid = 0
    mtotal = 0
    xpmean = 0
    vpmean = 0
    xpsigma = 0
    vpsigma = 0

    do i_part=1,npart_tot
       if(levelp_global(i_part) > 0) then
          i_part_valid = i_part_valid+1
          mtotal = mtotal + mp_global(i_part_valid)
          i_dim = 2 ! arbitrarily chosen dimension; sanity check only
          xpmean = xpmean + xp_global(i_part_valid,i_dim)
          xpsigma = xpsigma + xp_global(i_part_valid,i_dim)**2
          vpmean = vpmean + vp_global(i_part_valid,i_dim)
          vpsigma = vpsigma + vp_global(i_part_valid,i_dim)**2
       endif
    enddo
    xpmean = xpmean/real(i_part_valid,dp)
    if(i_part_valid > 1) &
         xpsigma = sqrt(xpsigma/real(i_part_valid,dp) - xpmean*xpmean)*sqrt(real(i_part_valid,dp)/real(i_part_valid-1,dp))
    vpmean = vpmean/real(i_part_valid,dp)
    if(i_part_valid > 1) &
         vpsigma = sqrt(vpsigma/real(i_part_valid,dp) - vpmean*vpmean)*sqrt(real(i_part_valid,dp)/real(i_part_valid-1,dp))

    write(*,'(a)')'amr_dtfe_interface_sanity_check:'
    write(*,'(a,i12,a,es13.5,a,2f13.5,a,2es13.5)')'i_part_valid valid = ',&
         i_part_valid, &
         ' mtotal = ',mtotal, &
         '  xp mean stdev ',xpmean, xpsigma, &
         '  vp mean stdev ',vpmean, vpsigma

    ! calculate Omega_R map
    call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
    call units_kpc_Gyr(scale_l_kpc,scale_t_Gyr,scale_v_kpcGyr, &
         scale_hexp_invGyr, scale_hexp_kmsMpc)

    convert_Q = (scale_v_kpcGyr/scale_l_kpc)**2
    write(*,'(a,es13.5)')&
         'sim units will be multiplied by convert_Q = ',convert_Q

    Q_mean = 0
    Q_sigma = 0
    do ip = 1, dtfe_n_gridsize
       do jp = 1, dtfe_n_gridsize
          do kp = 1, dtfe_n_gridsize
             Q_mean = Q_mean + Q_kin_backreaction(ip,jp,kp)
             Q_sigma = Q_sigma + Q_kin_backreaction(ip,jp,kp)**2
          enddo
       enddo
    enddo
    Q_mean = Q_mean/real(dtfe_n_gridsize**ndim,dp)
    if(dtfe_n_gridsize > 1) &
         Q_sigma = &
         sqrt(Q_sigma/real(dtfe_n_gridsize**ndim,dp) - Q_mean**2) &
         *sqrt(real(dtfe_n_gridsize**ndim,dp)/ &
         real(dtfe_n_gridsize**ndim-1,dp))
    write(*,'(a,2es13.5)') &
         '  Q_mean Q_sigma ',Q_mean*convert_Q, Q_sigma*convert_Q

    rho_mean = 0
    rho_sigma = 0
    do ip = 1, dtfe_n_gridsize
       do jp = 1, dtfe_n_gridsize
          do kp = 1, dtfe_n_gridsize
             rho_mean = rho_mean + dtfe_density(ip,jp,kp)
             rho_sigma = rho_sigma + dtfe_density(ip,jp,kp)**2
          enddo
       enddo
    enddo
    rho_mean = rho_mean/real(dtfe_n_gridsize**ndim,dp)
    if(dtfe_n_gridsize > 1) &
         rho_sigma = &
         sqrt(rho_sigma/real(dtfe_n_gridsize**ndim,dp) - rho_mean**2) &
         *sqrt(real(dtfe_n_gridsize**ndim,dp)/ &
         real(dtfe_n_gridsize**ndim-1,dp))
    write(*,'(a,2es13.5)') &
         '  rho_mean rho_sigma ',rho_mean, rho_sigma


  end subroutine amr_dtfe_interface_sanity_check

#endif /* SCALAV_CHECK_AMR_DTFE_INTERFACE */

end subroutine amr_dtfe_interface
