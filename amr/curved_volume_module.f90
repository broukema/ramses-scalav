! curved_volume - convert flat volume to curved, using curvature radius
! Copyright (C) 2016 B. Roukema  under the Cecill licence
! See http://www.cecill.info for licence details; the licence is
! GPL-compatible.


! Given a Euclidean 3-volume vol_flat, calculate the effective radius,
! i.e. the radius assuming that the 3-volume is bounded by a 2-sphere,
! and then output the spherical or hyperbolic volume to that radius.
! A negative return value indicates an error.

module curved_volume_module
  use amr_parameters, only : dp
  real(dp), parameter :: PI = acos(-1.0D0) !3.14159265359
  real(dp), parameter :: INVALID_INPUT = -9e9

contains

  function curved_volume_3D(vol_flat, R_C, sign_RC)
    implicit none
    ! OUTPUT (function name)
    real(dp) :: curved_volume_3D ! volume corrected from flat to curved

    ! INPUTS
    real(dp), intent(in) :: vol_flat !  Euclidean 3D volume
    real(dp), intent(in) :: R_C      !  curvature radius (absolute real value)
    integer, intent(in)  :: sign_RC     !  -1, 0, or +1 (the 0 case gives output=input)

    ! INTERNAL
    real(dp) :: radius ! radius of equivalent 2-sphere

    ! Calculate the effective radius, i.e. the radius of a 2-sphere
    ! with the input 3-volume.
    radius =  exp((1.0_dp/3.0_dp)*log(vol_flat * 0.75_dp/PI))

    select case(sign_RC)
    case(0)
       ! flat case - trivial
       curved_volume_3D = vol_flat
    case(-1)
       ! hyperbolic case
       curved_volume_3D = PI * R_C**2 &
            * (R_C * sinh(2.0_dp * radius/R_C) - 2.0_dp * radius)
    case(1)
       ! spherical case (aka elliptical case)
       curved_volume_3D = PI * R_C**2 &
            * (2.0_dp * radius - R_C * sin(2.0_dp * radius/R_C) )
    case default
       curved_volume_3D = INVALID_INPUT
    endselect

    return
  endfunction curved_volume_3D

endmodule curved_volume_module
