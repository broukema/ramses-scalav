! Copyright (C) 2010-2016 Romain Teyssier et al under the Cecill licence
! See http://www.cecill.info for licence details.
! Co-holders of copyright for this file include Andreas Bleuler,
! Tstranex, Boud Roukema.

subroutine units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
  use amr_commons
  use hydro_commons
  use cooling_module
  real(dp)::scale_nH,scale_T2,scale_t,scale_v,scale_d,scale_l
  !-----------------------------------------------------------------------
  ! Conversion factors from user units into cgs units
  ! For gravity runs, make sure that G=1 in user units.
  !-----------------------------------------------------------------------

  ! scale_d converts mass density from user units into g/cc
  scale_d = units_density
  if(cosmo)then
     if(.not.scalav .or. .not.have_a_D_estimate)then
        scale_d = omega_m * rhoc *(h0/100.)**2 / aexp**3
     else ! if scalav
        scale_d = omega_m * rhoc *(h0/100.)**2 / a_D_common**3
     endif
  endif

  ! scale_t converts time from user units into seconds
  scale_t = units_time
  ! cosmo: convert main simulation time units into
  ! Shandarin--Martel--Shapiro supercomoving seconds
  if(cosmo) scale_t = aexp**2 / (h0*1d5/3.08567758d24)

  ! scale_l converts distance from user units into cm
  scale_l = units_length
  if(cosmo)then
     if(.not.scalav .or. .not.have_a_D_estimate)then
        scale_l = aexp * boxlen_ini * 3.08567758d24 / (h0/100)
     else ! if scalav
        scale_l = a_D_common * boxlen_ini * 3.08567758d24 / (h0/100)
     endif
  endif
  
  ! scale_v converts velocity in user units into cm/s
  scale_v = scale_l / scale_t

  ! scale_T2 converts (P/rho) in user unit into (T/mu) in Kelvin
  scale_T2 = mH/kB * scale_v**2

  ! scale_nH converts rho in user units into nH in H/cc
  scale_nH = X/mH * scale_d

end subroutine units

! Since "units" does not quite convert to cgs units in the
! cosmo=.true. case, this routine provides factors for
! scaling to kpc, Gyr and kpc/Gyr.

subroutine units_kpc_Gyr(scale_l_kpc,scale_texp_Gyr,scale_v_kpcGyr, scale_hexp_invGyr,scale_hexp_kmsMpc)
  use amr_commons

  implicit none

  !OUTPUTS:
  real(dp)::scale_l_kpc,scale_texp_Gyr,scale_v_kpcGyr
  real(dp)::scale_hexp_invGyr, scale_hexp_kmsMpc

  !INTERNAL:
  real(dp)::scale_nH,scale_T2,scale_t,scale_v,scale_d,scale_l
  
  ! 1 Mpc in terms of cm:
  real(dp), parameter :: Mpc_cm = 3.08567758d24
  ! 1e9 Julian years in terms of s:
  real(dp), parameter :: JulianGyr_s = 31557600d9
  ! 1 Mpc/Gyr in terms of km/s
  real(dp), parameter :: kms_per_MpcGyr = 977.79_dp

  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! This is for multiplying by texp.
  ! [check: compare getAgeGyr in amr/update_time.f90]
  scale_texp_Gyr = scale_t/aexp**2 /JulianGyr_s

  scale_l_kpc = scale_l / Mpc_cm *1000.0d0

  scale_v_kpcGyr = scale_l_kpc / scale_t * JulianGyr_s

  ! These are for multiplying by hexp.
  scale_hexp_invGyr = JulianGyr_s / scale_t
  scale_hexp_kmsMpc = scale_hexp_invGyr * kms_per_MpcGyr

end subroutine units_kpc_Gyr

